#include "project.h"

class pathDatabase
{
	private:
		unordered_map< uint64_t, path > database;
	public:
		pathDatabase();
		const path* find( point start, point end );
		bool update(path *newPath);  // add new Path if it is shorter -- return true if updated
		void read(FILE *file);
		void write(FILE* file) const; // make sure this is a new file everytime
};