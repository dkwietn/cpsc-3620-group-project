#ifndef PROJECTMAP_H
#define PROJECTMAP_H

#include "project.h"

class projectMap
{
	private:
		unsigned int height;
		unsigned int width;
		pixel **Map;
	public:
		projectMap();
		projectMap(unsigned int Height, unsigned int Width, unsigned int *mapArray);
		~projectMap();
		void read(FILE *file);
		void write(FILE* file) const;
		bool isRestricted(point P);
		void setVisited(point P);
};
#endif