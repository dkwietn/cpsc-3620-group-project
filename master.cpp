#include "project.h"

master::master(int Size, point Start, char *inputMapFileName, char *inputTopographicMapFileName, string PathDatabaseFileName)
{
	size = Size;
	startingPoint = Start;
	pathFileName = PathDatabaseFileName;
	// give everyone a map
	unsigned int dimensions[2];
	FILE *inputMapFile = fopen(inputMapFileName, "r");
	originalMap.read(inputMapFile);
	originalMap.setStart(startingPoint);
	fclose(inputMapFile);
	dimensions[0] = originalMap.getHeight();
	dimensions[1] = originalMap.getWidth();
	MPI_Bcast(dimensions, 2, MPI_UNSIGNED, MASTER, MPI_COMM_WORLD);
	unsigned int* mapArray = originalMap.toArray();
	MPI_Bcast(mapArray, dimensions[0] * dimensions[1], MPI_UNSIGNED, MASTER, MPI_COMM_WORLD);
	free(mapArray);
	
	// give everyone a topographic map
	inputMapFile = fopen(inputTopographicMapFileName, "r");
	elevationMap.read(inputMapFile);
	fclose(inputMapFile);
	dimensions[0] = elevationMap.getHeight();
	dimensions[1] = elevationMap.getWidth();
	MPI_Bcast(dimensions, 2, MPI_UNSIGNED, MASTER, MPI_COMM_WORLD);
	mapArray = elevationMap.toArray();
	MPI_Bcast(mapArray, dimensions[0] * dimensions[1], MPI_UNSIGNED, MASTER, MPI_COMM_WORLD);
	free(mapArray);
	
	path *tmpPath = new path(startingPoint);
	//pathDB.update(tmpPath);
	
	workload tmpWorkload;
	tmpWorkload.size = tmpPath->getSize();
	pathfinder chappie(tmpPath, &originalMap);
	unsigned char decision = chappie.getDecisionOptions();
	for(int i = 0; i < 8; i++)
	{
		if( decision & (1 << i))
		{
			tmpWorkload.locationArray = tmpPath->toLocationArray();
			tmpWorkload.directionArray = tmpPath->toDirectionArray();
			tmpWorkload.directionArray[tmpWorkload.size - 1] = i; 
			workStack.push(tmpWorkload);
		}
	}
	delete tmpPath;
}

void master::work()
{		
	// receive buffers
	point *locationArrayBuffer = (point*)malloc(sizeof(point));
	assert(locationArrayBuffer != NULL);
	unsigned char *directionArrayBuffer = (unsigned char*)malloc(sizeof(unsigned char));
	assert(directionArrayBuffer != NULL);
	
	// distribute some work
	bool workArray[size]; // keep track of which nodes have work
	memset(workArray, false, sizeof(bool) * size);
	workArray[0] = true; // master node always has work -- makes computation simpler later
	int index = 1; // which worker needs to be assigned work
	int freeWorkerCount = size - 1;
	int currentWorkDistributed = 0;
	unsigned int totalWorkCompleted = 0;
	workload tmpWorkload;
	
	while(currentWorkDistributed > 0 || !workStack.empty())
	{
		while( !workStack.empty() )
		{	
			// determine which worker to send to
			if(freeWorkerCount == 0) // all workers are busy
			{
				index = receiveWork();
				workArray[index] = false; // worker currently does not have work
				currentWorkDistributed--;
				totalWorkCompleted++;
				freeWorkerCount++;
			}
			else while( workArray[index = (index + 1) % size] ); // find available worker
			
			//send work
			tmpWorkload = workStack.front();
			workStack.pop();
			MPI_Send( tmpWorkload.locationArray, 2 * tmpWorkload.size, MPI_UNSIGNED, index, WORKLOAD, MPI_COMM_WORLD );
			MPI_Send( tmpWorkload.directionArray, tmpWorkload.size, MPI_UNSIGNED_CHAR, index, WORKLOAD, MPI_COMM_WORLD );
			free( tmpWorkload.locationArray );
			free( tmpWorkload.directionArray );
			currentWorkDistributed++;
			workArray[index] = true;
			freeWorkerCount--;
		}
		index = receiveWork();
		workArray[index] = false;
		currentWorkDistributed--;
		totalWorkCompleted++;
		freeWorkerCount++;
	}
	
	// send stop signal
	locationArrayBuffer = (point*)malloc(sizeof(point));
	assert(locationArrayBuffer != NULL);
	directionArrayBuffer = (unsigned char *)malloc(sizeof(unsigned char));
	assert(directionArrayBuffer != NULL);
	for(int i = 1; i < size; i++)
	{
		MPI_Send(locationArrayBuffer, 2, MPI_UNSIGNED, i, NO_WORK_LEFT, MPI_COMM_WORLD);
		MPI_Send(directionArrayBuffer, 1, MPI_UNSIGNED_CHAR, i, NO_WORK_LEFT, MPI_COMM_WORLD);
	}
	free(locationArrayBuffer);
	free(directionArrayBuffer);
	
	printf("Total Work Completed = %u\n", totalWorkCompleted);
	//printf("PATH in master: %s\n", pathDatabaseFileName);

	FILE *outputPathDatabaseFile = fopen(pathFileName.c_str(), "w");
	pathDB.write(outputPathDatabaseFile);
	fclose(outputPathDatabaseFile);
}

int master::receiveWork()
{
	MPI_Status status;
	int count = 0;
	unsigned int distance = 0;
	// receive worker's coordinates
	MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status );
	MPI_Get_count(&status, MPI_UNSIGNED, &count);
	locationArrayBuffer = (point*)malloc(sizeof(unsigned int) * count);
	assert(locationArrayBuffer != NULL);
	MPI_Recv(locationArrayBuffer, count, MPI_UNSIGNED, status.MPI_SOURCE, status.MPI_TAG, MPI_COMM_WORLD, &status);
	
	// receive worker's currently made decisions
	MPI_Probe(status.MPI_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status );
	MPI_Get_count(&status, MPI_UNSIGNED_CHAR, &count);
	directionArrayBuffer = (unsigned char *) malloc(sizeof(unsigned char) * count);
	assert(directionArrayBuffer != NULL);
	MPI_Recv(directionArrayBuffer, count, MPI_UNSIGNED_CHAR, status.MPI_SOURCE, status.MPI_TAG, MPI_COMM_WORLD, &status);
	
	// receive worker's distance travelled
	MPI_Recv(&distance, 1, MPI_UNSIGNED, status.MPI_SOURCE, status.MPI_TAG, MPI_COMM_WORLD, &status);
	
	// receive worker's decision options
	decisionBuffer = (unsigned char)status.MPI_TAG;

	// record path
	path *tmpPath = new path(locationArrayBuffer, directionArrayBuffer, count, distance, decisionBuffer);
	if(pathDB.update(tmpPath)) // if new path is shorter write out new database and add branches
	{	
		if(decisionBuffer != 0) // not a deadend
		{
			// need to branch on possible decisions
			workload tmpWorkload;
			tmpWorkload.size = count;
			for(int i = 0; i < 8; i++)
			{
				// decisionBuffer{bit} == valid/invalid 
				if( decisionBuffer & (1 << i)) 	// make decision and add workload back to stack
				{
					tmpWorkload.locationArray = (point *)malloc(sizeof(point) * count);
					memcpy( tmpWorkload.locationArray, locationArrayBuffer, sizeof(point) * count);
					tmpWorkload.directionArray = (unsigned char *)malloc(sizeof(unsigned char) * count);
					memcpy( tmpWorkload.directionArray, directionArrayBuffer, sizeof(unsigned char) * count );
					tmpWorkload.directionArray[tmpWorkload.size - 1] = i; 
					workStack.push(tmpWorkload);
				}
			}
		} else free(locationArrayBuffer);
	}
	delete tmpPath;
	free(directionArrayBuffer);
	return status.MPI_SOURCE;
}
