#include "project.h"

building buildingDatabase::find(string name)
{
	building Building(name);
	return *(database.find(Building));
}

void buildingDatabase::read(FILE *file)
{
	building newBuilding;
	char buf[100];
	while(fgets(buf, 100, file) != NULL)
	{
		newBuilding.read(buf);
		database.insert(newBuilding);
	}
}

void buildingDatabase::process(int size) 
{
	point startingPoint;
	master *M;
	int message = WORKLOAD;
	for (std::set<building, buildingComparator>::iterator i=database.begin(); i != database.end(); i++) 
	{
		startingPoint = i->getCoordinate();
		string fileName = "Output/" + (i->getName()) + ".txt";
		//send work mesage
		MPI_Bcast(&message, 1, MPI_INT, MASTER, MPI_COMM_WORLD);
		M = new master(size, startingPoint, const_cast<char*>("Maps/Project_Path_Map.ppm"), const_cast<char*>("Maps/Project_Topographic_Map.ppm"), fileName);
		M->work();
		delete M;
	}
	//send no work message
	message = NO_WORK_LEFT;
	MPI_Bcast(&message, 1, MPI_INT, MASTER, MPI_COMM_WORLD);
}

void buildingDatabase::outputMaps()
{
	pathDatabase *pDatabase;
	FILE *fp;
	const path *p;

	
	// read in list of buildings to use as starting points to draw maps
	fp = fopen("Maps/buildingSubsetDatabase.txt", "r");
	set<building, buildingComparator> bds;
	building newBuilding;
	char buffer[100];
	while(fgets(buffer, 100, fp) != NULL)
	{
		newBuilding.read(buffer);
		bds.insert(newBuilding);
	}
	fclose(fp);
	
	for (std::set<building, buildingComparator>::iterator i=bds.begin(); i != bds.end(); i++) 
	{		
		string fileName = "Output/" + (i->getName()) + ".txt";
		puts(fileName.c_str());
		fp = fopen(fileName.c_str(), "r");
		pDatabase = new pathDatabase();
		pDatabase->read( fp );
		fclose(fp);
		for (std::set<building, buildingComparator>::iterator j=database.begin(); j != database.end(); j++)
		{
			p = pDatabase->find( i->getCoordinate(), j->getCoordinate() );
			if( p != NULL )
			{

				fileName = "Output/" + (i->getName()) + "_to_" + (j->getName()) + ".ppm";
				puts(fileName.c_str());
				path *newPath = new path(p->toLocationArray(), p->toDirectionArray(), p->getSize());
				convertToRealWorldMap(newPath, fileName);
			}
		}
		delete pDatabase;
	}
}
