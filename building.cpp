#include "project.h"

building::building()
{

}

building::building(string Name)
{
	name = Name;
}

void building::read(char* buffer)
{
	char *ptr;

	//x coordinate
	ptr = strtok(buffer, "\t");
	coordinate.x = atoi(ptr);
//	printf("X: %d ", coordinate.x);

	//y coordinate
	ptr = strtok(NULL, "\t");
	coordinate.y = atoi(ptr);
//	printf("Y: %d ", coordinate.y);

	//name
	ptr = strtok(NULL, "\n");
	name = ptr;
	name.erase(name.length(), 1);
//	printf("NAME: %s\n", name.c_str());
}

string building::getName() const
{
	return name;
}

point building::getCoordinate() const
{
	return coordinate;
}
