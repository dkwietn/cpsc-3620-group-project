#ifndef PATHFINDER_H
#define PATHFINDER_H

#include "project.h"

class pathfinder
{
	private:
		point position;
		unsigned int pixelCount;
		unsigned int currentElevation;
		unsigned int elevationChange;
		pathMap *workingMap;
		topographicMap *elevationMap;
		path *workingPath;
		
		
		void makeDecision(unsigned char decision);
		void continueAlongPath();
		bool getNextPosition();
		bool goDown(point *current);
		bool goUp(point *current);
		bool goLeft(point *current);
		bool goRight(point *current);
		bool goDownLeft(point *current);
		bool goDownRight(point *current);
		bool goUpLeft(point *current);
		bool goUpRight(point *current);
	public:
		pathfinder( path *P, pathMap *M );
		pathfinder( path *P, pathMap *M, topographicMap *T );
		void tracePath();
		unsigned char getDecisionOptions();
		
};
#endif
