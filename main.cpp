#include "project.h"
#include <unistd.h>

int main(int argc, char *argv[])
{
	int size;
	int rank;
	
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	// debug information
	// mpiexec -n 2 ./main
	// open new terminal
	// this program will print out PID
	// connect to it with gdb
	// sudo  gdb -p <PID>
	// once inside gdb, keep pressing n until  while (0 == i) is reached
	// execute below command in gdb
	// set var i = 1
	// program will continue execution and can be debugged normally
	

    printf("rank = %d, PID %d\n", rank, getpid());
	//int i = 0;
    fflush(stdout);
   // while (0 == i)
    //    sleep(5);
		
	// remove above debugging code
	
	if( rank == 0 )
	{
		FILE* file = fopen("Maps/buildingDatabase.txt", "r");
		buildingDatabase bd;
		//Populate building database
		bd.read(file);
		fclose(file);
		//loop to find starting point
		//point startingPoint;
		//startingPoint.x = 704;
		//startingPoint.y = 539;

		double startTime = MPI_Wtime();
		bd.process(size);
		//master M(size, startingPoint, const_cast<char*>("Maps/Project_Path_Map.ppm"), const_cast<char*>("Maps/Project_Topographic_Map.ppm"), const_cast<char*>("704-539.txt"));
		//M.work();
		startTime = MPI_Wtime() - startTime;
		printf("Total Execution Time Was %f Seconds With %d Workers\n", startTime, size - 1);
	}
	else
	{
		int term;
		for(;;) 
		{
			//Get work message
			MPI_Bcast(&term, 1, MPI_INT, MASTER, MPI_COMM_WORLD);
			if(term == 0) 
				break;
			workerNodeJob(rank);
		}
	}

	MPI_Finalize();
	return 0;
}
