#ifndef PATHMAP_H
#define PATHMAP_H

#include "project.h"

class pathMap
{
	private:
		unsigned int height;
		unsigned int width;
		pixel **Map;
	public:
		pathMap();
		pathMap(unsigned int Height, unsigned int Width, unsigned int *mapArray);
		~pathMap();
		void read(FILE *file);
		void write(FILE* file) const;
		void analyze();
		pixel* getPixel(point P);
		void asciiPrint();
		unsigned int *toArray();
		unsigned int getHeight();
		unsigned int getWidth();
		bool isRestricted(point P);
		bool isRoad(point P);
		bool isBuilding(point P);
		bool isFootPath(point P);
		bool isDoor(point P);
		bool isVisited(point P);
		bool isEnd(point P);
		bool isStart(point P);
		void setVisited(point P);
		void setStart(point P);
};
#endif