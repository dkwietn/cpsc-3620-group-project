#include "project.h"

pathfinder::pathfinder( path *P, pathMap *M )
{
	workingPath = P;
	workingMap = M;
	elevationMap = NULL;
	position = workingPath->getStartingPoint();
	pixelCount = 0;
	elevationChange = 0;
	currentElevation = 0;
}

pathfinder::pathfinder( path *P, pathMap *M, topographicMap *T )
{
	workingPath = P;
	workingMap = M;
	elevationMap = T;
	position = workingPath->getStartingPoint();
	pixelCount = 0;
	elevationChange = 0;
	currentElevation = elevationMap->getElevationAtPoint(position);
}

void pathfinder::tracePath()
{
	const list<decision> *directions = workingPath->getDirections();
	for (list<decision>::const_iterator i = directions->begin(); i != directions->end(); i++)
	{
		makeDecision(i->direction);
		continueAlongPath();
	}
	workingPath->addDecision(position);
	workingPath->setPixelsTraversed(pixelCount + (elevationChange)/2);
}

void pathfinder::makeDecision(unsigned char decision)
{
	switch(decision)
	{
	case(UP):
		goUp(&position);
		break;
	case(DOWN):
		goDown(&position);
		break;
	case(LEFT):
		goLeft(&position);
		break;
	case(RIGHT):
		goRight(&position);
		break;
	case(UP_LEFT):
		goUpLeft(&position);
		break;
	case(UP_RIGHT):
		goUpRight(&position);
		break;
	case(DOWN_LEFT):
		goDownLeft(&position);
		break;
	case(DOWN_RIGHT):
		goDownRight(&position);
		break;
	default:
		return;
	}
	
	workingMap->setVisited(position);
	pixelCount++;
	if(elevationMap != NULL)
	{
		unsigned newElevation = elevationMap->getElevationAtPoint(position);
		elevationChange += (abs(((int)currentElevation) - ((int)newElevation)));
		currentElevation = newElevation;
	}
}

void pathfinder::continueAlongPath()
{
	while(getNextPosition())
	{
		workingMap->setVisited(position);
		pixelCount++;
		if(elevationMap != NULL)
		{
			unsigned newElevation = elevationMap->getElevationAtPoint(position);
			elevationChange += (abs(((int)currentElevation) - ((int)newElevation)));
			currentElevation = newElevation;
		}
	}
}

bool pathfinder::getNextPosition()
{
	point next[2];
	bool isDecision = true;
	unsigned char decision = UNKNOWN;
	for(int i = 0; i < 2; i++)
	{
		next[i] = position;
		if(decision != UP && goUp(&next[i]))
		{
			decision = UP;
			continue;
		}
		if(decision != DOWN && goDown(&next[i]))
		{
			decision = DOWN;
			continue;
		}
		if(decision != LEFT && goLeft(&next[i]))
		{
			decision = LEFT;
			continue;
		}
		if(decision != RIGHT && goRight(&next[i]))
		{
			decision = RIGHT;
			continue;
		}
		if(decision != UP_LEFT && goUpLeft(&next[i]))
		{
			decision = UP_LEFT;
			continue;
		}
		if(decision != UP_RIGHT && goUpRight(&next[i]))
		{
			decision = UP_RIGHT;
			continue;
		}
		if( decision != DOWN_LEFT && goDownLeft(&next[i]))
		{
			decision = DOWN_LEFT;
			continue;
		}
		if(decision != DOWN_RIGHT && goDownRight(&next[i]))
		{
			decision = DOWN_RIGHT;
			continue;
		}
		isDecision = !isDecision; // two fails means deadend
	}
	
	if(isDecision)
		return false;
	else
	{
		position = next[0];
		return true;
	}
}

unsigned char pathfinder::getDecisionOptions()
{
	unsigned char options = 0;
	point next = position;
	if(goUp(&next))
	{
		next = position;
		options = options | (1 << UP);
	}
	if(goDown(&next))
	{
		next = position;
		options = options | (1 << DOWN);
	}
	if(goLeft(&next))
	{
		next = position;
		options = options | (1 << LEFT);
	}
	if(goRight(&next))
	{
		next = position;
		options = options | (1 << RIGHT);
	}
	if(goUpLeft(&next))
	{
		next = position;
		options = options | (1 << UP_LEFT);
	}
	if(goUpRight(&next))
	{
		next = position;
		options = options | (1 << UP_RIGHT);
	}
	if(goDownLeft(&next))
	{
		next = position;
		options = options | (1 << DOWN_LEFT);
	}
	if(goDownRight(&next))
	{
		next = position;
		options = options | (1 << DOWN_RIGHT);
	}
	return options;
}

bool pathfinder::goDown(point *current)
{
	current->y++;
	if( workingMap->isRestricted(*current))
	{
		current->y--;
		return false;
	}
	return true;
}

bool pathfinder::goUp(point *current)
{
	current->y--;
	if( workingMap->isRestricted(*current))
	{
		current->y++;
		return false;
	}
	return true;
}

bool pathfinder::goLeft(point *current)
{
	current->x--;
	if( workingMap->isRestricted(*current))
	{
		current->x++;
		return false;
	}
	return true;
}

bool pathfinder::goRight(point *current)
{
	current->x++;
	if( workingMap->isRestricted(*current))
	{
		current->x--;
		return false;
	}
	return true;
}

bool pathfinder::goDownLeft(point *current)
{
	current->x--;
	current->y++;
	if( workingMap->isRestricted(*current))
	{
		current->x++;
		current->y--;
		return false;
	}
	return true;
}

bool pathfinder::goDownRight(point *current)
{
	current->x++;
	current->y++;
	if( workingMap->isRestricted(*current))
	{
		current->x--;
		current->y--;
		return false;
	}
	return true;
}

bool pathfinder::goUpLeft(point *current)
{
	current->x--;
	current->y--;
	if( workingMap->isRestricted(*current))
	{
		current->x++;
		current->y++;
		return false;
	}
	return true;
}

bool pathfinder::goUpRight(point *current)
{
	current->x++;
	current->y--;
	if( workingMap->isRestricted(*current))
	{
		current->x--;
		current->y++;
		return false;
	}
	return true;
}