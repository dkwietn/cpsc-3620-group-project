using namespace std;
#ifndef PROJECT_H
#define PROJECT_H

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <math.h> 
#include <limits>
#include <list>
#include <set>
#include <stack>
#include <queue> 
#include <unordered_map>
#include <time.h>
#include <mpi.h>
#include <stdint.h>

#include "utilities.h"
#include "pixel.h"
#include "pathMap.h"
#include "topographicMap.h"
#include "projectMap.h"
#include "building.h"
#include "buildingDatabase.h"
#include "path.h"
#include "pathDatabase.h"
#include "master.h"
#include "pathfinder.h"
#endif