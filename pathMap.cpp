#include "project.h"

pathMap::pathMap()
{
	height = 0;
	width = 0;
}

// make a map based on an array of colors -- used after broadcast
pathMap::pathMap(unsigned int Height, unsigned int Width, unsigned int *mapArray)
{
	height = Height;
	width = Width;
	Map = new pixel*[height];
	for(unsigned int i = 0; i < height; i++)
	{
		Map[i] = new pixel[width];
		for(unsigned int j = 0; j < width; j++)
		{
			Map[i][j].setColor(*(mapArray + (i * width) + j));
		}
	}
}

pathMap::~pathMap()
{
	for(unsigned int i = 0; i < height; i++)
		delete[] Map[i];
	delete[] Map;
}

void pathMap::read(FILE *file)
{
	char temp[4];
	fgets(temp, 4, file);
	fscanf(file, "%d %d", &width, &height);
	fscanf(file, "%d\n", (int*)temp);
	Map = new pixel*[height];
	for(unsigned int i = 0; i < height; i++)
	{
		Map[i] = new pixel[width];
		for(unsigned int j = 0; j < width; j++)
		{
			Map[i][j].read(file);
		}
	}
}

void pathMap::write(FILE* file) const
{
	fprintf(file, "P6\n%d %d\n255\n", width, height);
	for(unsigned int i = 0; i < height; i++)
		for(unsigned int j = 0; j < width; j++)
			Map[i][j].write(file);
}

void pathMap::analyze()
{
	int doorCount = 0;
	int footPathCount = 0;
	point current;
	
	for(unsigned int i = 0; i < height; i++)
		for(unsigned int j = 0; j < width; j++)
		{
			current.x = j;
			current.y = i;
			if( isFootPath(current)) footPathCount++;
			else if( isDoor(current)) doorCount++;
		}
	printf("%d doors, %d footpaths\n", doorCount, footPathCount);
}

pixel* pathMap::getPixel(point P)
{
	return &Map[P.y][P.x];
}

void pathMap::asciiPrint()
{
	FILE* file = stderr;
	fputc('\n', file);
	fputc('\n', file);
	point current;
	for( unsigned int i = 0; i < height; i++ )
	{
		for( unsigned int j = 0; j < width; j++)
		{
			current.x = j;
			current.y = i;
			if( isRoad(current))
				fputc('=', file);
			else if( isBuilding(current))
				fputc('+', file);
			else if( isFootPath(current))
				fputc('=', file);
			else if( isDoor(current))
				fputc('#', file);
			else if( isVisited(current))
				fputc('*', file);
			else if( isStart(current))
				fputc('@', file);
			else fputc(' ', file);
		}
		fputc('\n', file);
	}
	fputc('\n', file);
	fputc('\n', file);
}

unsigned int pathMap::getHeight()
{
	return height;
}

unsigned int pathMap::getWidth()
{
	return width;
}

// returns pointer to array -- needs to be freed
unsigned int *pathMap::toArray()
{
	unsigned int *mapArray = (unsigned int *)malloc(sizeof(unsigned int) * height * width);
	assert(mapArray != NULL);
	for(unsigned int i = 0; i < height; i++)
	{
		for(unsigned int j = 0; j < width; j++)
		{
			*(mapArray + (i * width) + j) = Map[i][j].getColor();
		}
	}
	return mapArray;
}
 
bool pathMap::isRestricted(point P)
{
	if( P.x < 0 || P.x >= width)
		return true;
	if(P.y < 0 || P.y >= height)
		return true;
	return !isFootPath(P) && !isDoor(P);
}

bool pathMap::isRoad(point P)
{
	return (Map[P.y][P.x]).getColor() == 0xffffff;
}

bool pathMap::isBuilding(point P)
{
	return (Map[P.y][P.x]).getColor() == 0xf2eee1;
}

bool pathMap::isFootPath(point P)
{
	return (Map[P.y][P.x]).getColor() == 0x0000ff;
}

bool pathMap::isDoor(point P)
{
	return (Map[P.y][P.x]).getColor() == 0xff0000;
}

bool pathMap::isVisited(point P)
{
	return (Map[P.y][P.x]).getColor() == 0x9600cd;
}

bool pathMap::isStart(point P)
{
	return (Map[P.y][P.x]).getColor() == 0x0000cd;
}

void pathMap::setVisited(point P)
{
	(Map[P.y][P.x]).setColor( 0x9600cd );
}

void pathMap::setStart(point P)
{
	(Map[P.y][P.x]).setColor( 0x0000cd );
}
