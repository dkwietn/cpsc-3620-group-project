#include "project.h"

void pixel::read(FILE *file)
{
	unsigned char rgb[3];
	fscanf(file, "%c%c%c", &rgb[0], &rgb[1], &rgb[2]);
	color = (rgb[0] << 16) | (rgb[1] << 8) | rgb[2];
}

void pixel::write(FILE* file) const
{
	fprintf(file, "%c%c%c", (color & (0xff << 16)) >> 16, (color & (0xff << 8)) >> 8, (color & 0xff ));
}

unsigned int pixel::getColor()
{
	return color;
}

void pixel::setColor(unsigned int Color)
{
	color = Color;
}

void pixel::setColor( unsigned char r, unsigned char g, unsigned char b )
{
	color = (r << 8) | (g << 4) | b;
}