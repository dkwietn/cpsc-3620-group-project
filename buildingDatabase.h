#include "project.h"

struct buildingComparator
{
	bool operator()(building a, building b) const
	{
		return a.getName().compare(b.getName()) < 0;
	}
};

class buildingDatabase
{
	private:
		set<building, buildingComparator> database;
	public:
		building find(string name);
		void read(FILE *file);
		void process(int size);
		void outputMaps();
};