#ifndef MASTER_H
#define MASTER_H

#include "project.h"

class master
{
	private:
		int size;
		point startingPoint;
		point *locationArrayBuffer;
		unsigned char *directionArrayBuffer;
		unsigned char decisionBuffer;
		pathMap originalMap;
		topographicMap elevationMap;
		pathDatabase pathDB;
		string pathFileName;
		queue<workload> workStack;
		int receiveWork();
		
	public:
		master(int Size, point Start, char *inputMapFileName, char *inputTopographicMapFileName, string PathDatabaseFileName);
		void work();
};
#endif
