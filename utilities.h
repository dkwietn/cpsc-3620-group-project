#ifndef UTILITIES_H
#define UTILITIES_H

#include "project.h"
class path;
class pathMap;
// directions
#define UNKNOWN 255
#define DOWN 0
#define UP 1
#define LEFT 2
#define RIGHT 3
#define DOWN_LEFT 4
#define DOWN_RIGHT 5
#define UP_LEFT 6
#define UP_RIGHT 7


#define MASTER 0

// tags used by master
#define NO_WORK_LEFT 0
#define WORKLOAD 1

// generic struct to keep 
// track of a point on the map
typedef struct
{
	unsigned int x, y;
} point;

typedef struct
{
	point location;
	unsigned char direction;
} decision;


typedef struct
{
	point* locationArray;
	unsigned char* directionArray;
	unsigned int size;
} workload;

void workerNodeJob(int rank);
double calculateDistance(point a, point b);
bool pointsNotEqual(point a, point b);
unsigned int pixelsToRealWorld(unsigned int pixelCount); // converts Pixels to Feet
uint64_t hashPoint( point p );
void convertToRealWorldMap(path *Path, string outputMapName);
#endif
