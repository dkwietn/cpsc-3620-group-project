#include "project.h"
#include "utilities.h"

class building
{
	private:
		point coordinate;
		string name;
	public:
		building();
		building(string Name);
		string getName() const;
		point getCoordinate() const;
		void read(char *buffer);
};