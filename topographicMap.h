#ifndef TOPOGRAPHICMAP_H
#define TOPOGRAPHICMAP_H

#include "project.h"

typedef struct
{
	unsigned int distance;
	unsigned int elevation;
} elevationInfo;

class topographicMap
{
	private:
		unsigned int height;
		unsigned int width;
		pixel **Map;
		
		elevationInfo searchUp(point P);
		elevationInfo searchDown(point P);
		elevationInfo searchLeft(point P);
		elevationInfo searchRight(point P);
		elevationInfo searchDownLeft(point P);
		elevationInfo searchDownRight(point P);
		elevationInfo searchUpLeft(point P);
		elevationInfo searchUpRight(point P);
	public:
		topographicMap();
		topographicMap(unsigned int Height, unsigned int Width, unsigned int *mapArray);
		~topographicMap();
		void read(FILE *file);
		void write(FILE* file) const;
		pixel* getPixel(point P);
		void asciiPrint();
		unsigned int getHeight();
		unsigned int getWidth();
		unsigned int *toArray();
		unsigned int getElevationAtPoint(point P);  // complicated algorithm
		unsigned int getElevationOfPoint(point P);  // get elevation based on color
		unsigned int getElevationByColor( unsigned int color );
		bool isRestricted(point P);
		bool isContourLine(point P);
};
#endif