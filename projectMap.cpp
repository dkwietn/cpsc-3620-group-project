#include "project.h"

projectMap::projectMap()
{
	height = 0;
	width = 0;
}

// make a map based on an array of colors -- used after broadcast
projectMap::projectMap(unsigned int Height, unsigned int Width, unsigned int *mapArray)
{
	height = Height;
	width = Width;
	Map = new pixel*[height];
	for(unsigned int i = 0; i < height; i++)
	{
		Map[i] = new pixel[width];
		for(unsigned int j = 0; j < width; j++)
		{
			Map[i][j].setColor(*(mapArray + (i * width) + j));
		}
	}
}

projectMap::~projectMap()
{
	for(unsigned int i = 0; i < height; i++)
		delete[] Map[i];
	delete[] Map;
}

void projectMap::read(FILE *file)
{
	char temp[4];
	fgets(temp, 4, file);
	fscanf(file, "%d %d", &width, &height);
	fscanf(file, "%d\n", (int*)temp);
	Map = new pixel*[height];
	for(unsigned int i = 0; i < height; i++)
	{
		Map[i] = new pixel[width];
		for(unsigned int j = 0; j < width; j++)
		{
			Map[i][j].read(file);
		}
	}
}

void projectMap::write(FILE* file) const
{
	fprintf(file, "P6\n%d %d\n255\n", width, height);
	for(unsigned int i = 0; i < height; i++)
		for(unsigned int j = 0; j < width; j++)
			Map[i][j].write(file);
}

void projectMap::setVisited(point P)
{
	(Map[P.y][P.x]).setColor( 0x9600cd );
}

bool projectMap::isRestricted(point P)
{
	if( P.x < 0 || P.x >= width)
		return true;
	if(P.y < 0 || P.y >= height)
		return true;
	return false;
}