#include "project.h"

void workerNodeJob(int rank)
{
	MPI_Status status;
	int count = 0;
	
	// get map
	unsigned int dimensions[2];
	MPI_Bcast(dimensions, 2, MPI_UNSIGNED, MASTER, MPI_COMM_WORLD);
	unsigned int* originalMap = (unsigned int *)malloc(sizeof(unsigned int) * dimensions[0] * dimensions[1]);
	assert(originalMap != NULL);
	MPI_Bcast(originalMap, dimensions[0] * dimensions[1], MPI_UNSIGNED, MASTER, MPI_COMM_WORLD);


	// get topographicMap
	unsigned int topographicMapDimensions[2];
	MPI_Bcast(topographicMapDimensions, 2, MPI_UNSIGNED, MASTER, MPI_COMM_WORLD);
	unsigned int* originalTopographicMap = (unsigned int *)malloc(sizeof(unsigned int) * topographicMapDimensions[0] * topographicMapDimensions[1]);
	assert(originalTopographicMap != NULL);
	MPI_Bcast(originalTopographicMap, topographicMapDimensions[0] * topographicMapDimensions[1], MPI_UNSIGNED, MASTER, MPI_COMM_WORLD);
	topographicMap *elevationMap = new topographicMap(topographicMapDimensions[0], topographicMapDimensions[1], originalTopographicMap);
	free(originalTopographicMap);
	
	// initialize buffers
	unsigned char decisionBuffer = 0;
	point *locationArrayBuffer;
	unsigned char *directionArrayBuffer;
	unsigned int distanceBuffer = 0;
	
	pathMap *workingMap = NULL;
	path *workingPath = NULL;

	pathfinder *jedi;
	
	for(;;)
	{
		// get coordinates
		MPI_Probe(MASTER, MPI_ANY_TAG, MPI_COMM_WORLD, &status );
		MPI_Get_count(&status, MPI_UNSIGNED, &count);
		locationArrayBuffer = (point*)malloc(sizeof(point) * count);
		assert(locationArrayBuffer != NULL);
		MPI_Recv(locationArrayBuffer, count, MPI_UNSIGNED, status.MPI_SOURCE, status.MPI_TAG, MPI_COMM_WORLD, &status);
		
		// get directions
		MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status );
		MPI_Get_count(&status, MPI_UNSIGNED_CHAR, &count);
		directionArrayBuffer = (unsigned char *) malloc(sizeof(unsigned char) * count);
		assert(directionArrayBuffer != NULL);
		MPI_Recv(directionArrayBuffer, count, MPI_UNSIGNED_CHAR, status.MPI_SOURCE, status.MPI_TAG, MPI_COMM_WORLD, &status);
		
		if( status.MPI_TAG == NO_WORK_LEFT )
			break;
		
		// do Work
		workingMap = new pathMap(dimensions[0], dimensions[1], originalMap);
		workingPath = new path(	locationArrayBuffer, directionArrayBuffer, count );
		free(locationArrayBuffer);
		free(directionArrayBuffer);
		
		//jedi = new pathfinder(workingPath, workingMap);
		jedi = new pathfinder(workingPath, workingMap, elevationMap);
		
		jedi->tracePath();
		decisionBuffer = jedi->getDecisionOptions();
		delete jedi;
		delete workingMap;
		
		// send results
		locationArrayBuffer = workingPath->toLocationArray();
		directionArrayBuffer = workingPath->toDirectionArray();
		distanceBuffer = workingPath->getPixelsTraversed();
		MPI_Send(locationArrayBuffer, 2 * workingPath->getSize(), MPI_UNSIGNED, MASTER, (int)decisionBuffer, MPI_COMM_WORLD);
		MPI_Send(directionArrayBuffer, workingPath->getSize(), MPI_UNSIGNED_CHAR, MASTER, (int)decisionBuffer, MPI_COMM_WORLD);
		MPI_Send(&distanceBuffer, 1, MPI_UNSIGNED, MASTER, (int)decisionBuffer, MPI_COMM_WORLD);
		delete workingPath;
	}
	free(originalMap);
	delete elevationMap;
}
