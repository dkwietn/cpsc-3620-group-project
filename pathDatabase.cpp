#include "project.h"

pathDatabase::pathDatabase()
{
	database.rehash(50000);
}

const path* pathDatabase::find( point start, point end )
{
	std::unordered_map<uint64_t, path>::iterator p = database.find(hashPoint(end));
	if( p != database.end() )
		return &p->second;
	else
		return NULL;
}

bool pathDatabase::update(path *newPath)
{
	// make a fake path to speed up comparisons
	uint64_t key = hashPoint(newPath->getDestination());
	std::unordered_map<uint64_t, path>::iterator current = database.find(key);
	
	if( current == database.end())
	{
		database.insert( std::pair<uint64_t, path>(key, *newPath) );
		return true;
	}
	
	if(newPath->hasShorterPath(&current->second))
	{
		database.erase(current);
		database.insert( std::pair<uint64_t, path>(key, *newPath));
		return true;
	}
	
	// check if new path has more possible decision options
	if((newPath->getDecisionOptions() | current->second.getDecisionOptions()) > current->second.getDecisionOptions())
	{
		current->second.setDecisionOptions(newPath->getDecisionOptions() | current->second.getDecisionOptions());
		return true;
	}
	
	return false;
}

void pathDatabase::read(FILE *file)
{
	path *newPath;
	char *buffer = (char*)malloc(sizeof(char) * 102400);
	while(fgets(buffer, 10240, file) != NULL)
	{
		newPath = new path();
		newPath->read(buffer);
		database.insert( std::pair<uint64_t, path>(hashPoint(newPath->getDestination()), *newPath));
	}
	free(buffer);
}

void pathDatabase::write(FILE* file) const
{
	for (auto i = database.begin(); i != database.end(); i++)
		i->second.write(file);
}
