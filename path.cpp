#include "project.h"

path::path()
{
	;
}

path::path( point start, point end )
{
	decision d;
	d.location = start;
	d.direction = UNKNOWN;  
	directions.push_back(d);
	
	startingPoint = start;
	destination = end;
	totalDistance = 0.0;
	pixelsTraversed = 0;
	decisionOptions = 0;
}

path::path( point start )
{
	decision d;
	d.location = start;
	d.direction = UNKNOWN;  
	directions.push_back(d);
	
	startingPoint = start;
	destination = start;
	totalDistance = 0.0;
	pixelsTraversed = 0;
	decisionOptions = 0;
}

path::path(point *locationArray, unsigned char *directionArray, unsigned int size)
{
	pixelsTraversed = 0;
	totalDistance = 0.0;
	decision d;
	// make sure size > 0 before calling
	d.location = locationArray[0];
	d.direction = directionArray[0];
	directions.push_back(d);
	for(unsigned int i = 1; i < size; i++)
	{
		d.location = locationArray[i];
		d.direction = directionArray[i];
		directions.push_back(d);
		totalDistance += calculateDistance(locationArray[i], locationArray[i-1]);
	}
	startingPoint = locationArray[0];
	destination = locationArray[size - 1];
	decisionOptions = 0;
}

path::path(point *locationArray, unsigned char *directionArray, unsigned int size, unsigned int pixelDistance)
{
	pixelsTraversed = pixelDistance;
	totalDistance = 0.0;
	decisionOptions = 0;
	decision d;
	// make sure size > 0 before calling
	d.location = locationArray[0];
	d.direction = directionArray[0];
	directions.push_back(d);
	for(unsigned int i = 1; i < size; i++)
	{
		d.location = locationArray[i];
		d.direction = directionArray[i];
		directions.push_back(d);
		totalDistance += calculateDistance(locationArray[i], locationArray[i-1]);
	}
	startingPoint = locationArray[0];
	destination = locationArray[size - 1];	
}


path::path(point *locationArray, unsigned char *directionArray, unsigned int size, unsigned int pixelDistance, unsigned char possibleDecisions)
{
	pixelsTraversed = pixelDistance;
	totalDistance = 0.0;
	decision d;
	// make sure size > 0 before calling
	d.location = locationArray[0];
	d.direction = directionArray[0];
	directions.push_back(d);
	for(unsigned int i = 1; i < size; i++)
	{
		d.location = locationArray[i];
		d.direction = directionArray[i];
		directions.push_back(d);
		totalDistance += calculateDistance(locationArray[i], locationArray[i-1]);
	}
	startingPoint = locationArray[0];
	destination = locationArray[size - 1];
	
	decisionOptions = possibleDecisions;
}

point path::getStartingPoint()
{
	return startingPoint;
}

point path::getDestination()
{
	return destination;
}

double path::getTotalDistance() const
{
	return totalDistance;
}

const list<decision> *path::getDirections()
{
	return &directions;
}

unsigned int path::getDirectionCount() const
{
	return directions.size();
}

unsigned int path::getPixelsTraversed() const
{
	return pixelsTraversed;
}

void path::setPixelsTraversed(unsigned int pixelsCount)
{
	pixelsTraversed = pixelsCount;
}

unsigned int path::getSize() const
{
	return directions.size();
}

unsigned char path::getDecisionOptions() const
{
	return decisionOptions;
}

void path::setDecisionOptions( unsigned char possibleDecisions )
{
	decisionOptions = possibleDecisions;
}

// return true if Path has longer distance
// make sure start and end points are the same first
bool path::hasShorterPath( const path *Path ) const
{
	if( pixelsTraversed != Path->getPixelsTraversed() )
	{
		return (pixelsTraversed < Path->getPixelsTraversed());
	}
	else if(directions.size() != Path->getDirectionCount())
	{
		return (directions.size() < Path->getDirectionCount());
	}
	return false;
}

void path::addDecision(point location)
{
	decision d;
	d.location = location;
	d.direction = UNKNOWN;
	totalDistance += calculateDistance(d.location, directions.back().location);
	directions.push_back(d);
	destination = d.location;
}

void path::read(char *buffer)
{
	char *ptr;
	ptr = strtok(buffer, "\t");
	sscanf(ptr, "(%u,%u)", &startingPoint.x, &startingPoint.y);
	ptr = strtok(NULL, "\t");
	sscanf(ptr, "(%u,%u)", &destination.x, &destination.y);
	ptr = strtok(NULL, "\t");
	sscanf(ptr, "<%u>", &pixelsTraversed);
	
	decision d;
	while((ptr = strtok(NULL, "\t")) != NULL)
	{
		sscanf(ptr, "[(%u,%u) %u]", &d.location.x, &d.location.y, (unsigned*)&d.direction);
		totalDistance += calculateDistance(d.location, directions.back().location);;
		directions.push_back(d);
	}
}

void path::write(FILE* file) const
{
	fprintf(file, "(%u,%u)", startingPoint.x, startingPoint.y);
	fputc('\t', file);
	fprintf(file, "(%u,%u)", destination.x, destination.y);
	fprintf(file, "\t<%u>", pixelsTraversed);
	for (list<decision>::const_iterator i = directions.begin(); i != directions.end(); i++)
	{
		fprintf(file, "\t[(%u,%u) %u]", i->location.x, i->location.y, i->direction);
	}
	fputc('\n', file);
}

point *path::toLocationArray() const
{
	point *locationArray = (point *)malloc(sizeof(point) * directions.size());
	assert(locationArray != NULL);
	int j = 0;
	for (list<decision>::const_iterator i = directions.begin(); i != directions.end(); i++)
	{
		locationArray[j++] = i->location;
	}
	return locationArray;
}

unsigned char *path::toDirectionArray() const
{
	unsigned char *directionArray = (unsigned char *)malloc(sizeof(unsigned char) * directions.size());
	assert(directionArray != NULL);
	int j = 0;
	for (list<decision>::const_iterator i = directions.begin(); i != directions.end(); i++)
	{
		directionArray[j++] = i->direction;
	}
	return directionArray;
}

