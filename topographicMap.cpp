#include "project.h"

#define XOFFSET 404
#define YOFFSET 201

topographicMap::topographicMap()
{
	height = 0;
	width = 0;
}

// make a map based on an array of colors -- used after broadcast
topographicMap::topographicMap(unsigned int Height, unsigned int Width, unsigned int *mapArray)
{
	height = Height;
	width = Width;
	Map = new pixel*[height];
	for(unsigned int i = 0; i < height; i++)
	{
		Map[i] = new pixel[width];
		for(unsigned int j = 0; j < width; j++)
		{
			Map[i][j].setColor(*(mapArray + (i * width) + j));
		}
	}
}

topographicMap::~topographicMap()
{
	for(unsigned int i = 0; i < height; i++)
		delete[] Map[i];
	delete[] Map;
}

void topographicMap::read(FILE *file)
{
	char temp[4];
	fgets(temp, 4, file);
	fscanf(file, "%d %d", &width, &height);
	fscanf(file, "%d\n", (int*)temp);
	Map = new pixel*[height];
	for(unsigned int i = 0; i < height; i++)
	{
		Map[i] = new pixel[width];
		for(unsigned int j = 0; j < width; j++)
		{
			Map[i][j].read(file);
		}
	}
}

void topographicMap::write(FILE* file) const
{
	fprintf(file, "P6\n%d %d\n255\n", width, height);
	for(unsigned int i = 0; i < height; i++)
		for(unsigned int j = 0; j < width; j++)
			Map[i][j].write(file);
}

pixel* topographicMap::getPixel(point P)
{
	return &Map[P.y][P.x];
}

void topographicMap::asciiPrint()
{
	FILE* file = stderr;
	fputc('\n', file);
	fputc('\n', file);
	for( unsigned int i = 0; i < height; i++ )
	{
		for( unsigned int j = 0; j < width; j++)
		{
			switch(getElevationByColor(Map[i][j].getColor()))
			{
				case(600):
					fputc('0', file);
					break;
				case(620):
					fputc('1', file);
					break;
				case(640):
					fputc('2', file);
					break;
				case(660):
					fputc('3', file);
					break;
				case(680):
					fputc('4', file);
					break;
				case(700):
					fputc('5', file);
					break;
				case(720):
					fputc('6', file);
					break;
				case(740):
					fputc('7', file);
					break;
				case(760):
					fputc('8', file);
					break;
				case(780):
					fputc('9', file);
					break;
				case(800):
					fputc('9', file);
					break;
				case(820):
					fputc('a', file);
				case(840):
					fputc('b', file);
					break;
				default:
					fputc('*', file);
					break;	
			}
		}
		fputc('\n', file);
	}
	fputc('\n', file);
	fputc('\n', file);
}

unsigned int topographicMap::getHeight()
{
	return height;
}

unsigned int topographicMap::getWidth()
{
	return width;
}

// returns pointer to array -- needs to be freed
unsigned int *topographicMap::toArray()
{
	unsigned int *mapArray = (unsigned int *)malloc(sizeof(unsigned int) * height * width);
	assert(mapArray != NULL);
	for(unsigned int i = 0; i < height; i++)
	{
		for(unsigned int j = 0; j < width; j++)
		{
			*(mapArray + (i * width) + j) = Map[i][j].getColor();
		}
	}
	return mapArray;
}

unsigned int topographicMap::getElevationOfPoint( point P )
{
	return getElevationByColor(Map[P.y + YOFFSET][P.x + XOFFSET].getColor());
}

unsigned int topographicMap::getElevationAtPoint(point P)
{
	P.x += XOFFSET;
	P.y += YOFFSET;
	elevationInfo elevationData[8];
	int i = 0;
	for( i = 0; i < 8; i++)
		switch(i)
		{
			case(UP):
				elevationData[i] = searchUp(P);
				break;
			case(DOWN):
				elevationData[i] = searchDown(P);
				break;
			case(LEFT):
				elevationData[i] = searchLeft(P);
				break;
			case(RIGHT):
				elevationData[i] = searchRight(P);
				break;
			case(UP_LEFT):
				elevationData[i] = searchUpLeft(P);
				break;
			case(UP_RIGHT):
				elevationData[i] = searchUpRight(P);
				break;
			case(DOWN_LEFT):
				elevationData[i] = searchDownLeft(P);
				break;
			case(DOWN_RIGHT):
				elevationData[i] = searchDownRight(P);
				break;
			default:
				assert(0);
		}
			
		double elevations[4];
		unsigned elevationCategory = getElevationByColor(Map[P.y][P.x].getColor());
		if( elevationCategory == 0 ) // if this is a contour line
		{
			for( i = 0; i < 8; i++)
			{
				if( elevationCategory < elevationData[i].elevation )
					elevationCategory = elevationData[i].elevation;
			}
			elevationCategory -= 0;
		}
			
		unsigned totalDistance = 0;
		unsigned distances[4];
		
		//-------------------------------------------------------------------------------------------
		totalDistance = elevationData[UP].distance + elevationData[DOWN].distance;
		elevations[0] =  (double)((double) elevationData[UP].elevation * ((double)(totalDistance - elevationData[UP].distance) / totalDistance)) +
							(double)((double) elevationData[DOWN].elevation * ((double)(totalDistance - elevationData[DOWN].distance) / totalDistance));
		if( elevations[0] < elevationCategory ) 
			elevations[0] = elevationCategory;
		distances[0] = min( elevationData[UP].distance, elevationData[DOWN].distance );
		//-------------------------------------------------------------------------------------------
		totalDistance = elevationData[RIGHT].distance + elevationData[LEFT].distance;
		elevations[1] =  (double)((double) elevationData[RIGHT].elevation * ((double)(totalDistance - elevationData[RIGHT].distance) / totalDistance)) +
							(double)((double) elevationData[LEFT].elevation * ((double)(totalDistance - elevationData[LEFT].distance) / totalDistance));
		if( elevations[1] < elevationCategory ) 
			elevations[1] = elevationCategory;
		distances[1] = min( elevationData[RIGHT].distance, elevationData[LEFT].distance );
		//-------------------------------------------------------------------------------------------
		totalDistance = elevationData[DOWN_RIGHT].distance + elevationData[UP_LEFT].distance;
		elevations[2] =  (double)((double) elevationData[DOWN_RIGHT].elevation * ((double)(totalDistance - elevationData[DOWN_RIGHT].distance) / totalDistance)) +
							(double)((double) elevationData[UP_LEFT].elevation * ((double)(totalDistance - elevationData[UP_LEFT].distance) / totalDistance));
							
		if( elevations[2] < elevationCategory ) 
			elevations[2] = elevationCategory;
		distances[2] = min( elevationData[DOWN_RIGHT].distance, elevationData[UP_LEFT].distance );
		//-------------------------------------------------------------------------------------------
		totalDistance = elevationData[UP_RIGHT].distance + elevationData[DOWN_LEFT].distance;
		elevations[3] =  (double)((double) elevationData[UP_RIGHT].elevation * ((double)(totalDistance - elevationData[UP_RIGHT].distance) / totalDistance)) +
							(double)((double) elevationData[DOWN_LEFT].elevation * ((double)(totalDistance - elevationData[DOWN_LEFT].distance) / totalDistance));
		if( elevations[3] < elevationCategory ) 
			elevations[3] = elevationCategory;
		distances[3] = min( elevationData[UP_RIGHT].distance, elevationData[DOWN_LEFT].distance );
		//-------------------------------------------------------------------------------------------
		
		//-------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------
		totalDistance = distances[0] + distances[1];
		elevations[0] = (double)((double) elevations[0] * ((double)(totalDistance - distances[0]) / totalDistance)) + \
						(double)((double) elevations[1] * ((double)(totalDistance - distances[1]) / totalDistance));
		distances[0] = min( distances[0], distances[1] );
		//-------------------------------------------------------------------------------------------
		totalDistance = distances[2] + distances[3];
		elevations[1] = (double)((double) elevations[2] * ((double)(totalDistance - distances[2]) / totalDistance)) + \
						(double)((double) elevations[3] * ((double)(totalDistance - distances[3]) / totalDistance));	
		distances[1] = min( distances[2], distances[3] );
		//-------------------------------------------------------------------------------------------
		
		//-------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------
		totalDistance = distances[0] + distances[1];
		elevations[0] = (double)((double) elevations[0] * ((double)(totalDistance - distances[0]) / totalDistance)) + \
						(double)((double) elevations[1] * ((double)(totalDistance - distances[1]) / totalDistance));			
		//elevations[0] = (elevations[0] + elevations[1] + elevations[2] + elevations[3])/4;
		return (unsigned)elevations[0];
}

unsigned int topographicMap::getElevationByColor( unsigned int color )
{
	switch(color)
	{
		case(0x000000):
			return 580;
		case(0x111111):
			return 600;
		case(0x222222):
			return 620;
		case(0x333333):
			return 640;
		case(0x444444):
			return 660;
		case(0x555555):
			return 680;
		case(0x666666):
			return 700;
		case(0x777777):
			return 720;
		case(0x888888):
			return 740;
		case(0x999999):
			return 760;
		case(0xaaaaaa):
			return 780;
		case(0xbbbbbb):
			return 800;
		case(0xcccccc):
			return 820;
		case(0xdddddd):
			return 840;
		case(0xeeeeee):
			return 860;
		case(0xffffff):
			return 880;
		case( 0xa67442 ): // Contour Line
			return 0;
		default:
			return std::numeric_limits<unsigned int>::max();
	}
}

elevationInfo topographicMap::searchUp(point P)
{
	unsigned int color = Map[P.y][P.x].getColor();
	elevationInfo elevationData;
	elevationData.distance = 0;
	while( !isRestricted(P) && color == Map[P.y][P.x].getColor())
	{
		P.y--;
		elevationData.distance++;
	}
	if(!isRestricted(P))
		color = Map[P.y][P.x].getColor();

	elevationData.elevation = getElevationByColor(color);
	return elevationData;
}

elevationInfo topographicMap::searchDown(point P)
{
	unsigned int color = Map[P.y][P.x].getColor();
	elevationInfo elevationData;
	elevationData.distance = 0;
	while( !isRestricted(P) && color == Map[P.y][P.x].getColor())
	{
		P.y++;
		elevationData.distance++;
	}
	if(!isRestricted(P))
		color = Map[P.y][P.x].getColor();

	elevationData.elevation = getElevationByColor(color);
	return elevationData;
}

elevationInfo topographicMap::searchLeft(point P)
{
	unsigned int color = Map[P.y][P.x].getColor();
	elevationInfo elevationData;
	elevationData.distance = 0;
	while( !isRestricted(P) && color == Map[P.y][P.x].getColor())
	{
		P.x--;
		elevationData.distance++;
	}
	if(!isRestricted(P))
		color = Map[P.y][P.x].getColor();

	elevationData.elevation = getElevationByColor(color);
	return elevationData;
}

elevationInfo topographicMap::searchRight(point P)
{
	unsigned int color = Map[P.y][P.x].getColor();
	elevationInfo elevationData;
	elevationData.distance = 0;
	while( !isRestricted(P) && color == Map[P.y][P.x].getColor())
	{
		P.x++;
		elevationData.distance++;
	}
	if(!isRestricted(P))
		color = Map[P.y][P.x].getColor();

	elevationData.elevation = getElevationByColor(color);
	return elevationData;
}

elevationInfo topographicMap::searchDownLeft(point P)
{
	unsigned int color = Map[P.y][P.x].getColor();
	elevationInfo elevationData;
	elevationData.distance = 0;
	while( !isRestricted(P) && color == Map[P.y][P.x].getColor())
	{
		P.x--;
		P.y++;
		elevationData.distance++;
	}
	if(!isRestricted(P))
		color = Map[P.y][P.x].getColor();

	elevationData.elevation = getElevationByColor(color);
	return elevationData;
}

elevationInfo topographicMap::searchDownRight(point P)
{
	unsigned int color = Map[P.y][P.x].getColor();
	elevationInfo elevationData;
	elevationData.distance = 0;
	while( !isRestricted(P) && color == Map[P.y][P.x].getColor())
	{
		P.x++;
		P.y++;
		elevationData.distance++;
	}
	if(!isRestricted(P))
		color = Map[P.y][P.x].getColor();

	elevationData.elevation = getElevationByColor(color);
	return elevationData;
}

elevationInfo topographicMap::searchUpLeft(point P)
{
	unsigned int color = Map[P.y][P.x].getColor();
	elevationInfo elevationData;
	elevationData.distance = 0;
	while( !isRestricted(P) && color == Map[P.y][P.x].getColor())
	{
		P.x--;
		P.y--;
		elevationData.distance++;
	}
	if(!isRestricted(P))
		color = Map[P.y][P.x].getColor();

	elevationData.elevation = getElevationByColor(color);
	return elevationData;
}

elevationInfo topographicMap::searchUpRight(point P)
{
	unsigned int color = Map[P.y][P.x].getColor();
	elevationInfo elevationData;
	elevationData.distance = 0;
	while( !isRestricted(P) && color == Map[P.y][P.x].getColor())
	{
		P.x++;
		P.y--;
		elevationData.distance++;
	}
	if(!isRestricted(P))
		color = Map[P.y][P.x].getColor();

	elevationData.elevation = getElevationByColor(color);
	return elevationData;
}

bool topographicMap::isRestricted(point P)
{
	if( P.x < 0 || P.x >= width)
		return true;
	if(P.y < 0 || P.y >= height)
		return true;
	return false;
}

bool topographicMap::isContourLine(point P)
{
	return (Map[P.y][P.x].getColor() == 0xa67442);
}
