#ifndef PIXEL_H
#define PIXEL_H
#include "project.h"

class pixel
{
	private:
		unsigned int color;
	public:
		void read(FILE *file);
		void write(FILE* file) const;
		unsigned int getColor();
		void setColor( unsigned int Color);
		void setColor( unsigned char r, unsigned char g, unsigned char b );
};
#endif
