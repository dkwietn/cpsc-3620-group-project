#include "project.h"

double calculateDistance(point a, point b)
{
	return sqrt((double)((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y)));
}

bool pointsNotEqual(point a, point b)
{
	return (a.x != b.x || a.y != b.y);
}

unsigned int pixelsToRealWorld(unsigned int pixelCount)
{
	return (200 * pixelCount) / 63;
}

uint64_t hashPoint( point p )
{
	uint64_t hash = (uint64_t)(((uint64_t)((uint64_t)(uint64_t)p.x) <<  16 ) | p.y );
	return hash;
}

void convertToRealWorldMap(path *Path, string outputMapName)
{
	pathMap *originalMap = new pathMap();
	FILE* projectMapFile = fopen(const_cast<char*>("Maps/Project_Path_Map.ppm"), "r");
	originalMap->read(projectMapFile);
	fclose(projectMapFile);

	originalMap->setStart(Path->getStartingPoint());
	pathfinder skinnyman(Path, originalMap);
	skinnyman.tracePath();
	
	projectMapFile = fopen("Maps/Project_Map.ppm", "r");
	projectMap outputMap;
	outputMap.read(projectMapFile);
	fclose(projectMapFile);
	
	unsigned int height = originalMap->getHeight();
	unsigned int width = originalMap->getWidth();

	point originalMapPoint;
	point outputMapPoint;
	
	for(unsigned int i = 0; i < height; i++)
		for(unsigned int j = 0; j < width; j++)
		{
			originalMapPoint.x = j;
			originalMapPoint.y = i;
			if(originalMap->isVisited( originalMapPoint ))
			{
				for( int k = -2; k < 2; k++)
					for( int l = -2; l < 2; l++ )
					{
						outputMapPoint.x = originalMapPoint.x + k;
						outputMapPoint.y = originalMapPoint.y + l;
						if(!outputMap.isRestricted( outputMapPoint ))
						{
							outputMap.setVisited(outputMapPoint);
						}
					}
			}
		}
	projectMapFile = fopen(outputMapName.c_str(), "w");
	outputMap.write(projectMapFile);
	fclose(projectMapFile);
}
