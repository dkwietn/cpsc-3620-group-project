#ifndef PATH_H
#define PATH_H

#include "project.h"

class path
{
	private:
		unsigned pixelsTraversed;
		point startingPoint;
		point destination;
		double totalDistance;
		unsigned char decisionOptions;
		list<decision> directions;
		
	public:
		path();
		path(point start);
		path( point start, point end );
		path(point *locationArray, unsigned char *directionArray, unsigned int size);
		path(point *locationArray, unsigned char *directionArray, unsigned int size, unsigned int pixelDistance);
		path(point *locationArray, unsigned char *directionArray, unsigned int size, unsigned int pixelDistance, unsigned char possibleDecisions);
		point getStartingPoint();
		point getDestination();
		double getTotalDistance() const;
		const list<decision> *getDirections();
		unsigned int getDirectionCount() const;
		unsigned int getPixelsTraversed() const;
		void setPixelsTraversed(unsigned int pixelsCount);
		unsigned char getDecisionOptions() const;
		void setDecisionOptions( unsigned char possibleDecisions );
		bool hasShorterPath( const path *Path ) const; // return true if Path has longer distance
		void addDecision( point location );
		unsigned int getSize() const;
		void read(char *buffer);
		void write(FILE* file) const;
		point *toLocationArray() const;
		unsigned char *toDirectionArray() const;
};
#endif