CC	= mpic++

INCLUDE	= -I .

# CFLAGS = -std=c++11 -Wall -g -Wfatal-errors
CFLAGS	= -std=c++11 -Wall -O2 -DNDEBUG -Wno-unused-result -Wfatal-errors

.cpp.o:
	$(CC) -c $(INCLUDE) $(CFLAGS) $<

OBJS = \
utilities.o \
pixel.o \
pathMap.o \
topographicMap.o \
projectMap.o \
building.o \
buildingDatabase.o \
path.o \
pathDatabase.o \
master.o \
worker.o \
pathfinder.o

main: main.cpp main.o $(OBJS)
	$(CC) $(CFLAGS) $(INCLUDE) -o $@ $@.o $(OBJS)

drawMaps: drawMaps.cpp drawMaps.o $(OBJS)
	$(CC) $(CFLAGS) $(INCLUDE) -o $@ $@.o $(OBJS)
	
clean:
	rm -f *.o
	rm -f main
	rm -f drawMaps
	rm -f *~